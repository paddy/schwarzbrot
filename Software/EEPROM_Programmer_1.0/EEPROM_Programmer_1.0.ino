int data_pins[8]={2,3,4,5,6,7,8,9};
#define p_clock 10
#define p_SR_serial 11
#define p_SR_OE 12
#define p_eeprom_WE 13
#define delay_set_add_EEPROM 500
#define delay_write_EEPROM 5000
#define delay_write_safety 5000
#define delay_read_EEPROM 1
#define delay_SR_clock 50
#define delay_setData 1


#define OP_ProgrammcounterIN 0x01
#define OP_ALUOUT 0x02
#define OP_RAMOUT 0x04
#define OP_RAMIN 0x08
#define OP_Register1OUT 0x10
#define OP_Register2OUT 0x20
#define OP_Register1BUS1IN 0x40
#define OP_Register1BUS2IN 0x80
#define OP_BefehlsregisterOUT 0x01
#define OP_RAMAdressregisterIN 0x02
#define OP_ADD 0x04
#define OP_SUB 0x08
#define OP_AND 0x10
#define OP_OR 0x20
#define OP_NOT 0x40
#define OP_ZwischenregisterReset 0x80

//

//#define program schwarzbrot_bin

#define program decoder_eeprom2

//
//byte program1[]={0,0,22,0b00111000,22,0b00110001,22,0b00101010,22,0b00100011,22,0b00011100,22,0b00010101,22,0b00100110,22,0b00000111,};

//#include "test.c"


byte opcode_test[]={0,255,1,255,2,255,3,255,4,255,5,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15,255,16,255,17,255,18,255,19,255,20,255,21,255,22,255,23,255};
//
byte decoder_eeprom1[]={
0, //000000 NOP
0, //000001 LOAD OP_ADD
0, //000010 STOP_ORE OP_ADD
OP_Register2OUT | OP_Register1BUS2IN, //000011 MOVE
OP_ProgrammcounterIN | OP_Register1OUT | OP_Register2OUT, //000100 JUMP 2 Register
OP_ProgrammcounterIN | OP_Register1OUT , // 000101 JUMP Reg1 + Immediate
OP_Register1OUT | OP_Register2OUT, //000110 OP_ADD Reg.
OP_Register1OUT ,               //000111 OP_ADD Imm.
OP_Register1OUT | OP_Register2OUT, //001000 OP_SUB Reg.
OP_Register1OUT ,               //001001 OP_SUB Imm.
OP_Register1OUT | OP_Register2OUT, //001010 OP_AND Reg.
OP_Register1OUT ,               //001011 OP_AND Imm.
OP_Register1OUT | OP_Register2OUT, //001100 NOP_AND Reg.
OP_Register1OUT ,               //001101 NOP_AND Imm.
OP_Register1OUT | OP_Register2OUT, //001110 OP_OR Reg.
OP_Register1OUT ,               //001111 OP_OR Imm.
OP_Register1OUT | OP_Register2OUT, //010000 NOP_OR Reg.
OP_Register1OUT ,               //010001 NOP_OR Imm.
OP_Register1OUT ,               //010010 OP_NOT Reg. 1
OP_Register1OUT ,               //010011 NOT imm
OP_Register1BUS2IN, //010100 LOAD Imm.
OP_Register2OUT, //010101 LOAD Reg.1(Reg.2)
OP_Register2OUT, //010110 STOP_ORE Reg.1(Reg.2)
0, //010111 Bedingter Sprung
0b11111111, //010111
0b11111111, //011000
0b11111111, //011001
0b11111111, //011010
0b11111111, //011100
0b11111111, //011101
0b11111111, //011110
0b11111111, //011111

// Zweiter Takt

[0x20] = 0, //100000 NOP
OP_RAMOUT | OP_Register1BUS1IN, //100001 LOAD OP_ADD
OP_RAMIN | OP_Register1OUT, //100010 STOP_ORE OP_ADD
0, //100011 MOVE
0, //000100 JUMP 2 Register
0, // 000101 JUMP Reg1 + Immediate
OP_ALUOUT | OP_Register1BUS1IN, //100110 OP_ADD Reg.
OP_ALUOUT | OP_Register1BUS1IN, //100111 OP_ADD Imm.
OP_ALUOUT | OP_Register1BUS1IN, //101000 OP_SUB Reg.
OP_ALUOUT | OP_Register1BUS1IN, //101001 OP_SUB Imm.
OP_ALUOUT | OP_Register1BUS1IN, //101010 OP_AND Reg.
OP_ALUOUT | OP_Register1BUS1IN, //101011 OP_AND Imm.
OP_ALUOUT | OP_Register1BUS1IN, //101100 NOP_AND Reg.
OP_ALUOUT | OP_Register1BUS1IN, //101101 NOP_AND Imm.
OP_ALUOUT | OP_Register1BUS1IN, //101110 OP_OR Reg.
OP_ALUOUT | OP_Register1BUS1IN, //101111 OP_OR Imm.
OP_ALUOUT | OP_Register1BUS1IN, //11000 NOP_OR Reg.
OP_ALUOUT | OP_Register1BUS1IN, //110001 NOP_OR Imm.
OP_ALUOUT | OP_Register1BUS1IN, //110010 OP_NOT Reg. 1
OP_ALUOUT | OP_Register1BUS1IN, //110011 OP_NOT Imm.

0, //110100 LOAD Imm.
OP_RAMOUT | OP_Register1BUS1IN, //110101 LOAD Reg.1(Reg.2)
OP_RAMIN | OP_Register1OUT, //110110 STOP_ORE Reg.1(Reg.2)
0, //110111 Bedingter Sprung
0xFE,
0,
0,
0,
0,
0,
0,
};




byte decoder_eeprom2[]={
0, //000000 NOP
OP_BefehlsregisterOUT | OP_RAMAdressregisterIN, //000001 LOAD OP_ADD
OP_BefehlsregisterOUT | OP_RAMAdressregisterIN, //000010 STOP_ORE OP_ADD
0, //000011 MOVE
0, //000100 JUMP 2 Register
OP_BefehlsregisterOUT , // 000101 JUMP Reg1 + Immediate
OP_ADD, //000110 OP_ADD Reg.
OP_ADD | OP_BefehlsregisterOUT,  //000111 OP_ADD Imm.
OP_ADD | OP_SUB, //001000 OP_SUB Reg.
OP_ADD | OP_SUB| OP_BefehlsregisterOUT, //001001 OP_SUB Imm.
OP_AND, //001010 OP_AND Reg.
OP_AND| OP_BefehlsregisterOUT, //001011 OP_AND Imm.
OP_AND | OP_NOT, //001100 NOP_AND Reg.
OP_AND | OP_NOT| OP_BefehlsregisterOUT, //001101 NOP_AND Imm.
OP_OR, //001110 OP_OR Reg.
OP_OR| OP_BefehlsregisterOUT, //001111 OP_OR Imm.
OP_OR | OP_NOT, //01000 NOP_OR Reg.
OP_OR | OP_NOT| OP_BefehlsregisterOUT, //010001 NOP_OR Imm.
OP_NOT,    //010010 OP_NOT Reg. 1
OP_NOT,    //010011 OP_NOT Reg. 1
OP_BefehlsregisterOUT, //010100 LOAD Imm.
OP_RAMAdressregisterIN, //010101 LOAD Reg.1(Reg.2)
OP_RAMAdressregisterIN, //010110 STOP_ORE Reg.1(Reg.2)
OP_ZwischenregisterReset, //010111 Bedingter Sprung 
0, //0x18
0,
0,
0,
0,
0,
0,
0,
0, //0x20
0,
0,
0,
0,
[0x25] = 0, // 000101 JUMP Reg1 + Immediate
};
int program_offset=0;
int program_len=sizeof(program);
void setup() {
  // put your setup code here, to run once:
  pinMode(p_SR_serial, OUTPUT);
  pinMode(p_clock, OUTPUT);
  pinMode(p_SR_OE, OUTPUT);
  pinMode(p_eeprom_WE, OUTPUT);
  digitalWrite(p_eeprom_WE, 0);
  digitalWrite(p_SR_OE, 0);
  Serial.begin(9600);
  
}

void loop() {  
  
  if(analogRead(A0)>500){
    Serial.println();
    Serial.println("Modus: Schreiben. Drücke den Taster um zu beginnen");
    Serial.print("Programmlänge: ");Serial.println(program_len);
    while(analogRead(A0)>500){
      if(analogRead(A1)>500){
        write_programm();
        Serial.println();
        Serial.println("Schreiben Beendet");
      }
    } 
  }
  else{
    Serial.println("Modus: Lesen. Drücke den Taster um zu beginnen");
    while(analogRead(A0)<500){
      if(analogRead(A1)>500){
        read_EEPROM();
        Serial.println();
        Serial.println("Lesen Beendet");
      }
    }   
    
  }
  
  
}
  
void read_EEPROM(){
  for(int i=0; i<8; i++){
    pinMode(data_pins[i], INPUT);
  }
  byte opcode;
  byte immediate1;
  byte immediate2;
  byte data;
  int program_len=sizeof(program);
  
  for (long i = 0; i < program_len; i++) {
    
   
    if(i%16==0){
      Serial.println();
      Serial.print(i,HEX);
      if(i<=0xFFF){
       Serial.print(" ");
      }
      if(i<=0xFF){
       Serial.print(" ");
      }
      if(i<=0xF){
       Serial.print(" ");
      }
      Serial.print("| ");    
    }
    if(analogRead(A2)>500){
      if(i%2==0 && i%16!=0){
        Serial.print("; ");
      }
    }
    set_add(i);     
    data = read_data();
    if(analogRead(A2)>500){       
      if(i%2==0){
        opcode = data & 0x1f;
        immediate1 = (data & 0b11100000) >> 5;
        Serial.print(opcode, HEX);
        if(opcode<=0xF){
          Serial.print(" ");
        }
        Serial.print(":");
        Serial.print(" ");
        Serial.print(immediate1, HEX);
        Serial.print(",");
      }
      else{
        immediate2 = data;
        int k=128;
        for(int j=0; j<7; j++ ){
          if(data < k){
            Serial.print("0");
          }        
          k = k/2;
        }
        Serial.print(immediate2, BIN);
      }
    }
    else{
      if(data < 0x10 ){
        Serial.print("0");
      }
      Serial.print(data, HEX);
      Serial.print(", ");
      
    }
   
    
  }
}

void write_programm(){
  for(int i=0; i<8; i++){
    pinMode(data_pins[i], OUTPUT);
  }
  Serial.println("Schreibvorgang. . .");
  long progress=0;
  for(int i=0; i<8; i++){
    pinMode(data_pins[i], OUTPUT);
  }
  for(int i=program_offset; i<program_len+program_offset; i++){ 
  //for(long i=0; i<=0x1FFF; i++){
    //program_len=0xFF;
    if(i%(program_len/10)==0){
      
      Serial.print(progress);Serial.println(" % Abgeschlossen");
      progress += 10;
    }
    set_add(i);    
    write_data(program[i]);   
    //write_data(0);
  }

}

void write_data(byte data){
  for(int i=0; i<8; i++){
    digitalWrite(data_pins[i], data & (1 << i));    
  }  
  delayMicroseconds(delay_setData);
  digitalWrite(p_eeprom_WE, 1);
  delayMicroseconds(delay_write_EEPROM);
  digitalWrite(p_eeprom_WE, 0);
  delayMicroseconds(delay_write_safety);
}


byte read_data(){ 
  int data=0;
  int multi=1;
  delayMicroseconds(delay_read_EEPROM);
  for(int i=0; i<8; i++){
    data=data+(multi*digitalRead(data_pins[i]));
    multi=multi*2;
  }

  return data;
}
void clock_pulse(){
  delayMicroseconds(delay_SR_clock);
  digitalWrite(p_clock, 1);
  delayMicroseconds(delay_SR_clock);
  digitalWrite(p_clock, 0);
}
void set_add(long add){
  //
  digitalWrite(p_SR_OE, 0);
  int add1=add%256;
  int add2=add/256;

   for(int i=7; i>=0; i--){

    digitalWrite(p_SR_serial, add2 & (1 << i) );
    clock_pulse();
  }
  
  for(int i=7; i>=0; i--){

    digitalWrite(p_SR_serial, add1 & (1 << i) );
    clock_pulse();
  } 
 clock_pulse();
 digitalWrite(p_SR_OE, 1);
 delayMicroseconds(delay_set_add_EEPROM);
}
