#ifndef MAIN_H
#define MAIN_H

#include "ports.h"
#include <stdbool.h>

#define analogHigh 500
#define analogMIN 0
#define analogMAX 1024

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static inline bool analog_is_high(int pin)
{
    return analogRead(pin) > analogHigh;
}

static inline bool button_pressed(int n)
{
    return analog_is_high(I_button_base + n);
}

void programer_main_menue(void);
void handle_io(bool *refresh__state);
void intr_io(void);

extern byte display_data_IO;

#endif
