#ifndef PORTS_H
#define PORTS_H

// -------------------PORTS--------------------

// Display
#define TFT_RST         48
#define TFT_DC          49
#define TFT_CS          53
#define TFT_MOSI        51
#define TFT_SCLK        52

// Clock Modul
#define IO_mainClock    10
#define IO_mainRST      11
#define I_clockspeed    A11
#define I_clockMode     A10 //1 = Auto

// IO Register
#define IO_data_IO      22
#define I_write_IO      44
#define I_read_IO       45
#define I_mainClock     43

// EEPROM Programmer
#define IO_eeprom_adr   30
#define IO_eeprom_data  2
#define IO_eeprom_WE    12
#define IO_eeprom_OE    13

// Knöpfe
#define I_button_base   A0
#define I_eepromWrite   A12
#define I_programMode   A13

#define I_singlePulse   (I_button_base + 0)
#define I_mainRST       (I_button_base + 1)

#endif
