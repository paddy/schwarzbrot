void display_init(void);
void display_clear(void);
void display_printf(const char *fmt, ...);

struct cursor {
    int x;
    int y;
};

void display_save_cursor(struct cursor* c);
void display_restore_cursor(struct cursor* c);

extern bool display_to_serial;
extern bool display_to_tft;
