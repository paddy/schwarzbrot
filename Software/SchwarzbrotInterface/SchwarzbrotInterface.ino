#include <SPI.h>
#include "TimerOne.h"
#include "main.h"
#include "display.h"

#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735

extern Adafruit_ST7735 tft;

//----------------------PARAMETER------------
//DISPLAY LAYOUT
#define layPosx_clockmode 30
#define layPosy_clockmode 10
#define layPosx_freq 55
#define layPosy_freq 20
#define layPosx_IO 50
#define layPosy_IO 30
//CLOCK
#define min_clockspeed 1
#define max_clockspeed 2500
#define singlePulse_duration 100000
#define delay_pulseButton 200000
#define delay_clockspeed_check 100000
#define clock_freq_tolerance 3
#define res_delay 100

//-----------------Globale Variablen----------------------
//GENERAL
int states[16];
//CLOCK
long mainClock_delay;
long mainClock_freq;
long last_clockspeed;
long clock_ts = micros();
long pulseButton_ts = micros();
long singlePulse_ts = micros();
bool autoclock = 1;
byte singlePulse = 0;
int last_I_singlePulse = 0;

static void print_state(void)
{
    tft.setCursor(0, 0);
    display_printf("Takt:      %s\n", autoclock ? "EIN" : "AUS");
    display_printf("Frequenz:  %4d\n", mainClock_freq);
    display_printf("IO-Reg:    %02x\n", display_data_IO);
}

void setup(void) {
    Serial.begin(9600);
    Serial.println("Serial Print Initialisiert");

    display_init();
    set_pins_main();
    main_RST();
    check_ports();
    Timer1.attachInterrupt(callback);
    print_state();
}

void loop() {
    bool refresh_state = false;

    check_ports();
    if(states[I_clockMode - A0] > analogHigh){
        if(autoclock == 0){
            Serial.println("Timer Gestartet");
            Timer1.start();
            autoclock = 1;
            refresh_state = true;
        }
    }
    else{
        if(singlePulse == 0 && autoclock == 1){
            Serial.println("Timer Gestoppt (1)");
            Timer1.stop();
            digitalWrite(IO_mainClock, 0);
            autoclock = 0;
            refresh_state = true;
        }
    }

    if(micros() - clock_ts >= delay_clockspeed_check){
        // Serial.print("last clockspeed:"); Serial.println(last_clockspeed);
        // Serial.print("current clockspeed:"); Serial.println(states[I_clockspeed - A0]);
        if(last_clockspeed - states[I_clockspeed - A0] > clock_freq_tolerance || states[I_clockspeed - A0] - last_clockspeed > clock_freq_tolerance){
            if(autoclock == 1){
                mainClock_delay = (long)1000000 / mainClock_freq;
                Timer1.initialize(mainClock_delay);
            }
            refresh_state = true;
            last_clockspeed = states[I_clockspeed - A0];
            clock_ts = micros();
        }
    }

  if(micros() - pulseButton_ts >= delay_pulseButton){
    if(states[I_singlePulse - A0] >= analogHigh){
      singlePulse = 2;
      pulseButton_ts = micros();    
    }       
  }
  if(singlePulse == 2){
    if(micros() - singlePulse_ts >= singlePulse_duration){
      digitalWrite(IO_mainClock, 1);
      singlePulse_ts = micros();
      singlePulse = 1;
    }
  }
  else if(singlePulse == 1){
    if(micros() - singlePulse_ts >= singlePulse_duration){
      digitalWrite(IO_mainClock, 0);
      singlePulse_ts = micros();
      singlePulse = 0;  
    }
  }
  if(states[I_mainRST - A0] > analogHigh){
    main_RST();  
  }

    handle_io(&refresh_state);

// EEPROM PROGRAMMER-------------------------------
  if(states[I_programMode - A0] < analogHigh){   
      Serial.println("Timer Gestoppt (2)");
      Timer1.stop();
      digitalWrite(IO_mainClock, 0);
      autoclock = 0;
      refresh_state = true;
      programer_main_menue();
  }

    if (refresh_state) {
        print_state();
    }
}

void callback(){
    static bool clk_level;

    intr_io();

    digitalWrite(IO_mainClock, clk_level);
    clk_level = !clk_level;
}


void main_RST(){
  if(states[I_mainRST - A0] > analogHigh){  
    digitalWrite(IO_mainRST, 0);
    digitalWrite(IO_mainClock, 1);
    delay(res_delay);
    digitalWrite(IO_mainClock, 0);
    delay(res_delay);
    digitalWrite(IO_mainClock, 1);
    delay(res_delay);
    digitalWrite(IO_mainClock, 0);
    delay(res_delay);
    digitalWrite(IO_mainRST, 1);
  }


}

void check_ports(){
  for(int i = 0; i < 16; i++){
    states[i] = analogRead(i + A0);  
  }
  
  mainClock_freq = map(states[I_clockspeed - A0], analogMIN, analogMAX, min_clockspeed, max_clockspeed);
}
void set_pins_main(){
  pinMode(IO_mainClock, 1);  
  pinMode(IO_mainRST, 1);
  pinMode(IO_eeprom_WE, 1);
  pinMode(IO_eeprom_OE, 1);
  digitalWrite(IO_mainRST, 1);
  digitalWrite(IO_eeprom_OE, 1);
  digitalWrite(IO_eeprom_WE, 1);
  for(int i = 0; i < 13; i++){
    pinMode(IO_eeprom_adr + i, OUTPUT);  
  }
}
