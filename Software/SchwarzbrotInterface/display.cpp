#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735

#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>

#include "display.h"
#include "ports.h"

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

#define BGCOLOR (0x1f | (0x1f << 11))

#define CHAR_WIDTH   6
#define CHAR_HEIGHT 12

bool display_to_serial = false;
bool display_to_tft = true;

void display_init(void)
{
    tft.initR(INITR_BLACKTAB);
    display_clear();
}

void display_clear(void)
{
    tft.fillScreen(BGCOLOR);
    tft.setCursor(0, 0);
}

void display_printf(const char *fmt, ...)
{
    va_list ap;
    char buf[64];
    char *pos, *old_pos;
    int ret;

    va_start(ap, fmt);
    ret = vsprintf(buf, fmt, ap);
    va_end(ap);

    if (ret < 0) {
        return;
    }

    if (display_to_tft) {
        tft.setTextColor(0x0, 0x1f | (0x3f << 5));
        tft.print(buf);
    }

    if (display_to_serial) {
        Serial.print(buf);
    }
}

void display_save_cursor(struct cursor* c)
{
    c->x = tft.getCursorX();
    c->y = tft.getCursorY();
}

void display_restore_cursor(struct cursor* c)
{
    tft.setCursor(c->x, c->y);
}
