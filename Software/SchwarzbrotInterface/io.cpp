#include <Arduino.h>
#include "main.h"

//IO REGISTER
bool clk = 0;
bool old_read_IO = 0;
byte curr_data_IO = 0;
byte display_data_IO = 0;
int read_state_IO = 0;

#define QUEUE_SIZE 128

enum {
    CMD_NONE            = 0x00,
    CMD_WRITE_NUMBER    = 0x01,
    CMD_WRITE_STR       = 0x02,
    CMD_MAX
};

struct queue {
    uint8_t data[QUEUE_SIZE];
    int head = 0;
    int tail = 0;
};

static void enqueue(struct queue *q, uint8_t val)
{
    int next = (q->head + 1) % sizeof(q->data);

    if (next != q->tail) {
        q->data[q->head] = val;
        q->head = next;
    }
}

static bool dequeue(struct queue *q, uint8_t *val)
{
    if (q->head == q->tail) {
        return false;
    }

    *val = q->data[q->tail];
    q->tail = (q->tail + 1) % sizeof(q->data);
    return true;
}

static struct queue read_queue;
static struct queue write_queue;

static void set_pins_IO(bool mode)
{
    for (int i = IO_data_IO; i < IO_data_IO+8; i++) {
        pinMode(i, mode);
    }
}

static byte read_data_IO()
{
    byte data = 0;

    for (int i = 0; i < 8; i++) {
        data |= digitalRead(i+IO_data_IO) << i;
    }

    return data;
}

static void write_data_IO(byte data)
{
    for (int i = 0; i < 8; i++) {
        digitalWrite(i+IO_data_IO, data & (1 << i));
    }
}

void intr_io(void)
{
    bool is_write_IO = digitalRead(I_write_IO);
    bool is_read_IO = digitalRead(I_read_IO);
    bool curr_clk = digitalRead(I_mainClock);

#if 0
    if (is_read_IO && !old_read_IO) {
        Serial.println("read -> 1");
        Serial.println(read_state_IO);
    } else if (!is_read_IO && old_read_IO) {
        Serial.println("read -> 0");
        Serial.println(read_state_IO);
    }
#endif

    if (is_write_IO) {
        set_pins_IO(0);
        curr_data_IO = read_data_IO();
        if (!clk && curr_clk) {
            enqueue(&write_queue,curr_data_IO);
        }
    }

    switch (read_state_IO) {
        case 0:
            if (is_read_IO) {
                uint8_t val;
                set_pins_IO(1);
                if (!dequeue(&read_queue, &val)) {
                    val = 0;
                }
                write_data_IO(val);
            }
            if (is_read_IO && !clk && curr_clk) {
                Serial.println("read && Steigende Flanke");
                read_state_IO = 1;
                old_read_IO = is_read_IO;

            }
            break;
        case 1:
            if (old_read_IO && clk && !curr_clk) {
                read_state_IO = 0;
                old_read_IO = is_read_IO;
            }
            break;
    }

#if 0
    if (clk != curr_clk) {
        Serial.print("Takt: ");
        Serial.println(curr_clk);
    }
#endif
    clk = curr_clk;
}

static struct {
    uint8_t cmd;
    uint8_t len;
    uint8_t pos;
} state;

void handle_io(bool *refresh_state)
{
    uint8_t val;

    while (dequeue(&write_queue, &val)) {
        switch (state.cmd) {
            case CMD_NONE:
                Serial.print('A');
                Serial.print('B');
                Serial.print('C');
                Serial.println(val);

                if (val < CMD_MAX) {
                    state.cmd = val;
                    state.len = 0;
                    state.pos = 0;
                }
                break;
            case CMD_WRITE_NUMBER:
                Serial.print("Wert: ");
                Serial.println(val);
                state.cmd = CMD_NONE;

                display_data_IO = val;
                *refresh_state = true;
                break;
            case CMD_WRITE_STR:
                display_data_IO = val;
                *refresh_state = true;

                if (state.len == 0) {
                    state.len = val;
                    if (val == 0) {
                        state.cmd = CMD_NONE;
                    }
                    break;
                }
                Serial.print((char) val);
                if (++state.pos == state.len) {
                    state.cmd = CMD_NONE;
                }
                break;
            default:
                Serial.print("Ungueltiger Befehl: ");
                Serial.println(state.cmd);
                state.cmd = CMD_NONE;
                break;
        }
        // Serial.println((write_queue.head - write_queue.tail + QUEUE_SIZE) % QUEUE_SIZE);
    }
}
