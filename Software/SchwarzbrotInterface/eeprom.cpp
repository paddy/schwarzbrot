// CPP
#include <Arduino.h>

#include "main.h"
#include "display.h"

#define delay_set_add_EEPROM 1
#define delay_write_EEPROM 1
#define delay_write_safety 1
#define delay_read_EEPROM 5
#define delay_setData 5
#define delay_key 200

#define OP_ProgrammcounterIN 0x01
#define OP_ALUOUT 0x02
#define OP_RAMOUT 0x04
#define OP_RAMIN 0x08
#define OP_Register1OUT 0x10
#define OP_Register2OUT 0x20
#define OP_Register1BUS1IN 0x40
#define OP_Register1BUS2IN 0x80
#define OP_BefehlsregisterOUT 0x01
#define OP_RAMAdressregisterIN 0x02
#define OP_ADD 0x04
#define OP_SUB 0x08
#define OP_AND 0x10
#define OP_OR 0x20
#define OP_NOT 0x40
#define OP_ZwischenregisterReset 0x80

byte decoder_eeprom1[]={
[0x00] = 0,                                         //000000 NOP
[0x01] = 0,                                         //000001 LOAD ADD
[0x02] = 0,                                         //000010 STORE ADD
[0x03] = OP_Register2OUT | OP_Register1BUS2IN,      //000011 MOVE
[0x04] = OP_ProgrammcounterIN |
         OP_Register1OUT |
         OP_Register2OUT,                           //000100 JUMP 2 Register
[0x05] = OP_ProgrammcounterIN | OP_Register1OUT ,   //000101 JUMP Reg1 + Immediate
[0x06] = OP_Register1OUT | OP_Register2OUT,         //000110 ADD Reg.
[0x07] = OP_Register1OUT ,                          //000111 ADD Imm.
[0x08] = OP_Register1OUT | OP_Register2OUT,         //001000 SUB Reg.
[0x09] = OP_Register1OUT ,                          //001001 SUB Imm.
[0x0a] = OP_Register1OUT | OP_Register2OUT,         //001010 AND Reg.
[0x0b] = OP_Register1OUT ,                          //001011 AND Imm.
[0x0c] = OP_Register1OUT | OP_Register2OUT,         //001100 NAND Reg.
[0x0d] = OP_Register1OUT ,                          //001101 NAND Imm.
[0x0e] = OP_Register1OUT | OP_Register2OUT,         //001110 OR Reg.
[0x0f] = OP_Register1OUT ,                          //001111 OR Imm.
[0x10] = OP_Register1OUT | OP_Register2OUT,         //010000 NOR Reg.
[0x11] = OP_Register1OUT ,                          //010001 NOR Imm.
[0x12] = OP_Register2OUT ,                          //010010 NOT Reg. 2
[0x13] = 0,                                         //010011 NOT Imm.
[0x14] = OP_Register1BUS2IN,                        //010100 LOAD Imm.
[0x15] = OP_Register2OUT,                           //010101 LOAD Reg.1(Reg.2)
[0x16] = OP_Register2OUT,                           //010110 STOP_ORE Reg.1(Reg.2)
[0x17] = 0,                                         //010111 Bedingter Sprung
[0x18] = 0,                                         //011000
[0x19] = 0,                                         //011001
[0x1a] = 0,                                         //011010
[0x1b] = 0,                                         //011011
[0x1c] = 0,                                         //011100
[0x1d] = 0,                                         //011101
[0x1e] = 0,                                         //011110
[0x1f] = 0,                                         //011111

// Zweiter Takt

[0x20] = 0,                              //100000 NOP
[0x21] = OP_RAMOUT | OP_Register1BUS1IN, //100001 LOAD ADD
[0x22] = OP_RAMIN | OP_Register1OUT,     //100010 STORE ADD
[0x23] = 0,                              //100011 MOVE
[0x24] = 0,                              //100100 JUMP 2 Register
[0x25] = 0,                              //100101 JUMP Reg1 + Immediate
[0x26] = OP_ALUOUT | OP_Register1BUS1IN, //100110 ADD Reg.
[0x27] = OP_ALUOUT | OP_Register1BUS1IN, //100111 ADD Imm.
[0x28] = OP_ALUOUT | OP_Register1BUS1IN, //101000 SUB Reg.
[0x29] = OP_ALUOUT | OP_Register1BUS1IN, //101001 SUB Imm.
[0x2a] = OP_ALUOUT | OP_Register1BUS1IN, //101010 AND Reg.
[0x2b] = OP_ALUOUT | OP_Register1BUS1IN, //101011 AND Imm.
[0x2c] = OP_ALUOUT | OP_Register1BUS1IN, //101100 NAND Reg.
[0x2d] = OP_ALUOUT | OP_Register1BUS1IN, //101101 NAND Imm.
[0x2e] = OP_ALUOUT | OP_Register1BUS1IN, //101110 OR Reg.
[0x2f] = OP_ALUOUT | OP_Register1BUS1IN, //101111 OR Imm.
[0x30] = OP_ALUOUT | OP_Register1BUS1IN, //110000 NOR Reg.
[0x31] = OP_ALUOUT | OP_Register1BUS1IN, //110001 NOR Imm.
[0x32] = OP_ALUOUT | OP_Register1BUS1IN, //110010 NOT Reg. 2
[0x33] = OP_ALUOUT | OP_Register1BUS1IN, //110011 NOT Imm.
[0x34] = 0,                              //110100 LOAD Imm.
[0x35] = OP_RAMOUT | OP_Register1BUS1IN, //110101 LOAD Reg.1(Reg.2)
[0x36] = OP_RAMIN | OP_Register1OUT,     //110110 STORE Reg.1(Reg.2)
[0x37] = 0,                              //110111 Bedingter Sprung
[0x38] = 0,                              //111000
[0x39] = 0,                              //111001
[0x3a] = 0,                              //111010
[0x3b] = 0,                              //111011
[0x3c] = 0,                              //111100
[0x3d] = 0,                              //111101
[0x3e] = 0,                              //111110
[0x3f] = 0,                              //111111
};


byte decoder_eeprom2[]={
[0x00] = 0,                                              //000000 NOP
[0x01] = OP_BefehlsregisterOUT | OP_RAMAdressregisterIN, //000001 LOAD ADD
[0x02] = OP_BefehlsregisterOUT | OP_RAMAdressregisterIN, //000010 STORE ADD
[0x03] = 0,                                              //000011 MOVE
[0x04] = 0,                                              //000100 JUMP 2 Register
[0x05] = OP_BefehlsregisterOUT ,                         //000101 JUMP Reg1 + Immediate
[0x06] = OP_ADD,                                         //000110 ADD Reg.
[0x07] = OP_ADD | OP_BefehlsregisterOUT,                 //000111 ADD Imm.
[0x08] = OP_ADD | OP_SUB,                                //001000 SUB Reg.
[0x09] = OP_ADD | OP_SUB| OP_BefehlsregisterOUT,         //001001 SUB Imm.
[0x0a] = OP_AND,                                         //001010 AND Reg.
[0x0b] = OP_AND| OP_BefehlsregisterOUT,                  //001011 AND Imm.
[0x0c] = OP_AND | OP_NOT,                                //001100 NAND Reg.
[0x0d] = OP_AND | OP_NOT| OP_BefehlsregisterOUT,         //001101 NAND Imm.
[0x0e] = OP_OR,                                          //001110 OR Reg.
[0x0f] = OP_OR| OP_BefehlsregisterOUT,                   //001111 OR Imm.
[0x10] = OP_OR | OP_NOT,                                 //010000 NOR Reg.
[0x11] = OP_OR | OP_NOT| OP_BefehlsregisterOUT,          //010001 NOR Imm.
[0x12] = OP_NOT,                                         //010010 NOT Reg. 2
[0x13] = OP_NOT | OP_BefehlsregisterOUT,                 //010011 NOT Imm.
[0x14] = OP_BefehlsregisterOUT,                          //010100 LOAD Imm.
[0x15] = OP_RAMAdressregisterIN,                         //010101 LOAD Reg.1(Reg.2)
[0x16] = OP_RAMAdressregisterIN,                         //010110 STORE Reg.1(Reg.2)
[0x17] = OP_ZwischenregisterReset,                       //010111 Bedingter Sprung
[0x18] = 0,                                              //011000
[0x19] = 0,                                              //011001
[0x1a] = 0,                                              //011010
[0x1b] = 0,                                              //011011
[0x1c] = 0,                                              //011100
[0x1d] = 0,                                              //011101
[0x1e] = 0,                                              //011110
[0x1f] = 0,                                              //011111

// Zweiter Takt

[0x20] = 0,                                              //100000
[0x21] = 0,                                              //100001
[0x22] = 0,                                              //100010
[0x23] = 0,                                              //100011
[0x24] = 0,                                              //100100
[0x25] = 0,                                              //100101
[0x26] = 0,                                              //100110
[0x27] = 0,                                              //100111
[0x28] = 0,                                              //101000
[0x29] = 0,                                              //101001
[0x2a] = 0,                                              //101010
[0x2b] = 0,                                              //101011
[0x2c] = 0,                                              //101100
[0x2d] = 0,                                              //101101
[0x2e] = 0,                                              //101110
[0x2f] = 0,                                              //101111
[0x30] = 0,                                              //110000
[0x31] = 0,                                              //110001
[0x32] = 0,                                              //110010
[0x33] = 0,                                              //110011
[0x34] = 0,                                              //110100
[0x35] = 0,                                              //110101
[0x36] = 0,                                              //110110
[0x37] = 0,                                              //110111
[0x38] = 0,                                              //111000
[0x39] = 0,                                              //111001
[0x3a] = 0,                                              //111010
[0x3b] = 0,                                              //111011
[0x3c] = 0,                                              //111100
[0x3d] = 0,                                              //111101
[0x3e] = 0,                                              //111110
[0x3f] = 0,                                              //111111
};

#define schwarzbrot_bin bin_alu
#include "asm/alu.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_ram
#include "asm/ram.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_test
#include "asm/test.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_incr
#include "asm/incr.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_cmp
#include "asm/cmp.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_ram_read
#include "asm/ram_read.c"
#undef schwarzbrot_bin

#define schwarzbrot_bin bin_io
#include "asm/io.c"
#undef schwarzbrot_bin

struct Program {
    const char *name;
    const unsigned char *bin;
    int len;
};

#define PROG(x, _name) { .name = _name, .bin = x, .len = sizeof(x) }

static const struct Program programs[] = {
    PROG(bin_test, "test.c"),
    PROG(bin_io, "io.c"),
    PROG(bin_alu, "alu.c"),
    PROG(bin_cmp, "cmp.c"),
    PROG(bin_incr, "incr.c"),
    PROG(bin_ram, "ram.c"),
    PROG(bin_ram_read, "ram_read.c"),
    PROG(decoder_eeprom1, "EEPROM 1"),
    PROG(decoder_eeprom2, "EEPROM 2"),
};

//------------------------------EEPROM PROGRAMMER------------------------------

static void EEPROM_set_add(int add)
{
    int add1 = add % 256;
    int add2 = add/256;
    //display_printf("Add1:");Serial.println(add);
    //display_printf("Add2:");Serial.println(add2);
    for(int i = 0; i < 8; i++) {
        digitalWrite(IO_eeprom_adr + i, add1 & (1 << i));
    }
    for(int i = 8; i < 13; i++) {
        digitalWrite(IO_eeprom_adr + i, add2 & (1 << (i-8)));
    }
}

static void EEPROM_set_data(byte data)
{
    for(int i=0; i < 8; i++) {
        digitalWrite(IO_eeprom_data + i, data & (1 << i));
    }
}

static void write_data(byte data) {
    EEPROM_set_data(data);
    //Serial.println(data, HEX);
    delay(delay_setData);
    digitalWrite(IO_eeprom_WE, 0);
    delay(delay_write_EEPROM);
    digitalWrite(IO_eeprom_WE, 1);
    delay(delay_write_safety);
}

static byte read_data() {
    int data=0;
    int multi=1;
    delay(delay_read_EEPROM);
    for(int i=0; i<8; i++) {
        data = data+(multi*digitalRead(IO_eeprom_data + i));
        multi=multi*2;
    }
    return data;
}

static void EEPROM_testWrite() {
    int data = 0;
    for(int i = 0; i < 8 ; i++) {
        pinMode(IO_eeprom_data + i, OUTPUT);
    }
    // EEPROM_clear();

    for(int i = 0; i <= 0x1FFF; i++) {
        if(i % 256 == 0 && i != 0) {
            data ++;
            display_printf("%d", i);
        }
        EEPROM_set_add(i);
        delay(delay_set_add_EEPROM);
        write_data((data % 256));
        //write_data(0xEE);
        data++;
    }

}
static void hybrid_print(int val) {
    display_printf("0x%04x(%05d)", val, val);
}


static void print_val(long *val, byte *split_val) {
    *val = 0;
    int fac = 0x1;
    for(int i = 0; i < 4; i++ ) {
        *val = *val + split_val[i]*fac;
        fac = fac*0x10;
    }
    hybrid_print(*val);
    display_printf("\n");
}

static void editor(const char *msg, int *val, int min_val, int max_val) {
    struct cursor c;

    display_clear();
    display_printf("%s\n", msg);
    display_printf("\n");
    display_printf("MIN: ");
    hybrid_print(min_val);
    display_printf("\nMAX: ");
    hybrid_print(max_val);
    display_printf("\n\nOK zum Bestatigen\n");

    display_save_cursor(&c);
    display_printf("\n\n> ");
    hybrid_print(*val);

    long c_val = *val;
    byte split_val[4];
    long cache = *val;

    for(int i = 0; i < 4; i++) {
        split_val[i] = cache % 0x10;
        cache = cache / 0x10;
    }
    delay(delay_key);

    while (!analog_is_high(I_programMode)) {
        for(int i = 0; i < 4; i++) {
            if (button_pressed(3-i) ) {
                split_val[i]++;
                if(split_val[i] == 0x10) {
                    split_val[i] = 0;
                }
                if(split_val[3] == 0x8) {
                    split_val[3] = 0;
                }
                display_restore_cursor(&c);
                display_printf("\n\n> ");
                print_val(&c_val, split_val);
                while (button_pressed(3-i)) {}
                delay(delay_key);
            }
        }
        if (button_pressed(8)) {
            if(c_val > max_val || c_val < min_val) {
                hybrid_print(c_val);
                display_printf(" ist ein Ungueltiger Wert!\n");
                while( button_pressed(8) ) {}
                delay(delay_key);
            }
            else{
                *val = c_val;
                while( button_pressed(8) ) {}
                return;
            }

        }
    }
}

static void edit_offset(int *val, int min_val, int max_val) {
    editor("Offset einstellen:", val, min_val, max_val);
}

static void edit_end(int *val, int min_val, int max_val) {
    editor("Ende einstellen:", val, min_val, max_val);
}

static void print_line_start(byte unit, long i) {
    display_printf("\n");
    if(unit == 16) {
        display_printf("0x%04x", i);
    } else {
        display_printf("%05d", i);
    }
    display_printf("| ");

}

static void print_data(byte data, bool mode, long i, byte unit) {
    byte opcode;
    byte immediate1;
    byte immediate2;
    if(mode == 1) {
        if(i%2==0) {
            opcode = data & 0x1f;
            immediate1 = (data & 0b11100000) >> 5;
            if(unit == 16) {
                display_printf("0x%02x", opcode);
            }
            else{
                display_printf("%02d", opcode);
            }
            display_printf(": %x,", immediate1);
        }
        else{
            immediate2 = data;
            for(int j=7; j>=0; j--) {
                display_printf("%d", !!(immediate2 & (1 << j)));
            }
            /*
               if(unit == 16) {
               display_printf("0x");
               if(immediate2 < 16) {
               display_printf("0");
               }
               display_printf(immediate2, HEX);
               }
               else{
               */
            display_printf("(%03d)", immediate2);
            //}
        }
    }
    else{
        if(unit == 16) {
            display_printf("0x%02x", data);
        }
        else{
            display_printf("%03d", data);
        }
        display_printf(", ");

    }
}

//unit 16: Hex; unit 10: dez; mode 0: dump, mode 1: Schwarzbrotdump
void read_EEPROM(int program_offset, int program_end, byte unit, bool mode) {
    digitalWrite(IO_eeprom_OE , 0);
    digitalWrite(IO_eeprom_WE , 1);
    if(analog_is_high(I_eepromWrite) ) {
        display_printf("Schalter in Falscher Stellung !\n");
        return;
    }
    for(int i=0; i < 8; i++) {
        pinMode(IO_eeprom_data + i, INPUT);
    }

    byte data;

    display_printf("\nWird gedruckt...\n");

    display_to_tft = false;
    for (long i = program_offset; i < program_end; i++) {
        if((unit == 16 && i%8==0) || (unit == 10 && i%10 == 0)) {
            print_line_start(unit, i);
        }
        if(mode == 1) {
            if(i%2==0) {
                if((unit == 16 && i%8!=0) || (unit == 10 && i%10 != 0)) {
                    display_printf("; ");
                }
            }
        }
        EEPROM_set_add(i);
        delay(delay_set_add_EEPROM);
        data = read_data();
        print_data(data, mode, i, unit);

    }
    display_to_tft = true;

    display_printf("\n");
    display_printf("Lesen Beendet\n");
    digitalWrite(IO_eeprom_OE , 1);
}

void write_program(int preset_data, int program_offset,int program_len,
                   const struct Program * program)
{
    //int program_len = program->len;
    struct cursor c;

    digitalWrite(IO_eeprom_OE , 1);
    digitalWrite(IO_eeprom_WE , 1);

    if (!analog_is_high(I_eepromWrite) || analog_is_high(I_programMode)) {
        display_printf("Schalter in falscher Stellung!\n");
        return;
    }

    for(int i=0; i<8; i++) {
        pinMode(IO_eeprom_data + i, OUTPUT);
    }
    display_printf("\nSchreibvorgang. . .\n");
    long last_progress=0;
    for(int i=0; i<8; i++) {
        pinMode(IO_eeprom_data + i, OUTPUT);
    }

    display_save_cursor(&c);
    display_printf("0%% abgeschlossen\n");

    for(int i=program_offset; i<program_len+program_offset; i++) {
        long cur = 100ul * i / (program_len - program_offset);
        if (cur > last_progress) {
            display_restore_cursor(&c);
            display_printf("%d%% abgeschlossen\n", cur);
            last_progress = cur;
        }
        EEPROM_set_add(i);
        delay(delay_set_add_EEPROM);
        if(preset_data != -1) {
            write_data(preset_data);
            //Serial.println(i);
        }
        else{
            write_data(program->bin[i]);
        }
    }

    display_restore_cursor(&c);
    display_printf("100%% abgeschlossen\n");
    display_printf("\n");
    display_printf("Schreiben beendet\n");
    digitalWrite(IO_eeprom_OE , 1);
    digitalWrite(IO_eeprom_WE , 1);
}

static void custom_write(int *program_offset)
{
    display_printf("\n");
    int dat = 0;
    edit_offset(program_offset, 0x0, 0x1FFF);
    int program_end = *program_offset;
    edit_end(&program_end, *program_offset, 0x1FFF);
    editor("Wert eingeben:", &dat, 0, 0xFF);

    display_printf("\n");
    hybrid_print(*program_offset);
    display_printf(" -> ");
    hybrid_print(program_end);
    display_printf(" WERT: ");
    hybrid_print(dat);
    display_printf("\n");

    display_printf("OK: Starten\n");
    display_printf("Andere Taste: Abbrechen\n");

    struct Program custom_prog = {
        .name = "",
        .bin = NULL,
        .len = program_end - *program_offset,
    };
    while(5) {
        if (button_pressed(4)) {
            write_program(dat, *program_offset, (&custom_prog)->len,&custom_prog );
            return;
        } else if (button_pressed(0) || button_pressed(1) ||
                   button_pressed(2) || button_pressed(3))
        {
            return;
        }
    }
}

void programer_main_menue(void)
{
    byte unit = 16;
    bool mode = 0;
    byte program_select = 0;
    int program_offset = 0;
    int program_len = programs[program_select].len;
    int program_end = program_offset + program_len;
    int read_len = program_len;

    digitalWrite(IO_eeprom_OE, 1);
    digitalWrite(IO_eeprom_OE, 1);

    display_to_serial = true;
    while (!analog_is_high(I_programMode)) {
        display_clear();
        if (analog_is_high(I_eepromWrite)) {
            program_len = programs[program_select].len;
            display_printf("+++++ Schreiben +++++\n");
            display_printf("%s\n", programs[program_select].name);
            display_printf("Offset: "); hybrid_print(program_offset); display_printf("\n");
            display_printf("Lange:  "); hybrid_print(program_len); display_printf("\n");
            display_printf("1: Offset aendern\n");
            display_printf("2: Custom Write\n");
            display_printf("3: EEPROM -> nop\n");
            display_printf("4: Programm zuruck\n");
            display_printf("5: Programm vor\n");
            display_printf("9: Schreiben starten\n");
            while (analog_is_high(I_eepromWrite) && !analog_is_high(I_programMode)) {
                if (button_pressed(0)) {
                    while( button_pressed(0)) {}
                    edit_offset(&program_offset, 0, 0x1FFF);
                    break;
                }
                else if (button_pressed(1)) {
                    while(button_pressed(1)) {}
                    custom_write(&program_offset);
                    break;
                }
                 else if (button_pressed(2)) {
                    while(button_pressed(2)) {}
                    write_program(0x19, 0, 0x1FFF,&programs[program_select]);
                    break;
                }
                else if (button_pressed(3)) {
                    while(button_pressed(3)) {}
                    program_select = (program_select - 1) % ARRAY_SIZE(programs);
                    program_len = programs[program_select].len;
                    read_len = program_len;

                    break;
                }
                
                else if (button_pressed(4)) {
                    while(button_pressed(4)) {}
                    program_select = (program_select + 1) % ARRAY_SIZE(programs);
                    program_len = programs[program_select].len;
                    program_end = program_offset + program_len;
                    read_len = program_len;
                    break;
                }

                else if (button_pressed(8)) {
                    write_program(-1, program_offset, (&programs[program_select])->len, &programs[program_select]);
                    //EEPROM_testWrite();
                    break;
                }
            }
        }
        else {
            digitalWrite(IO_eeprom_WE, 1);
            digitalWrite(IO_eeprom_OE, 1);

            display_printf("------- Lesen -------\n");
            if(unit == 16) {
                display_printf("Hex;");
            }
            else{
                display_printf("Dez;");
            }
            if(mode == 0) {
                display_printf(" Normal\n");
            }
            else{
                display_printf(" Schwarzbr.\n");
            }

            display_printf("Von:   ");
            hybrid_print(program_offset);
            display_printf("\nBis:   ");
            hybrid_print(program_end);
            display_printf("\nLange: ");
            hybrid_print(program_end - program_offset);
            display_printf("\n");

            display_printf("1: HEX/DEZ\n");
            display_printf("2: Normal/Schwarzbr.\n");
            display_printf("3: Offset andern\n");            
            display_printf("4: Ende andern\n");
            display_printf("5: EEPROM ausgeben\n");
            display_printf("9: Lesen beginnen\n");

            while (!analog_is_high(I_eepromWrite) &&
                   !analog_is_high(I_programMode))
            {
                if (button_pressed(0)) {
                    while(button_pressed(0)) {}
                    delay(delay_key);
                    if ( unit == 16) {
                        unit = 10;
                    }
                    else{
                        unit = 16;
                    }
                    break;
                }
                else if (button_pressed(1)) {
                    while(button_pressed(1)) {}
                    delay(delay_key);
                    mode = !mode;
                    break;
                }
                else if (button_pressed(2)) {
                    while(button_pressed(2)) {}
                    delay(delay_key);
                    edit_offset(&program_offset, 0, program_end);
                    program_end = program_offset + program_len;
                    break;
                }
                else if (button_pressed(3)) {
                    while(button_pressed(3)) {}
                    delay(delay_key);
                    edit_end(&program_end, program_offset, 0x1FFF);
                    read_len = program_end - program_offset;
                    break;
                }
                else if (button_pressed(4)) {
                    while(button_pressed(4)) {}
                    delay(delay_key);
                    program_offset = 0;
                    program_end = 0x1FFF;
                    break;
                }
                else if (button_pressed(8)) {
                    while(button_pressed(8)) {}
                    delay(delay_key);
                    read_EEPROM(program_offset, program_end, unit, mode);
                    break;
                }
            }
        }
    }

    display_to_serial = false;
}
