#define sen 500
#define t_safe 50
#define s_space 44
#define space 109
#define CTRL_port 11
#define SHIFT_port 12
#define p_serial_0 9
#define p_serial_1 10
#define led_capslock 13
#define c_break 176

#define t_timeout 3000
byte p_serial[2]{9,10};
bool b_caps;
bool b_ctrl;
bool b_shift;
void setup() {
  pinMode(led_capslock, 1);
  digitalWrite(led_capslock, b_caps);
  Serial.begin(9600);
}

void loop() {
    byte key=get_key();
    if (key!=0){
        Serial.print("Key: ");
        Serial.println(key);
        long dt=millis();
        if(key==39){
            b_caps=!b_caps;
            digitalWrite(led_capslock, b_caps);
            Serial.println("caps");
            while(get_key()==39){}
        }
        else{
            byte code=get_code(key, b_caps);
            send_serial(code);
            /*
               while(get_key()==key){
               send_serial(code);
               }
               send_serial(c_break);
             */
        }
        key=0;
    }
}

byte get_key(){
    int last_millis;
    byte key=0;
    byte i,j;
    if(millis()-last_millis<t_safe){return 0;}
    if((analogRead(A0)>sen) || (analogRead(A1)>sen) || (analogRead(A2)>sen) || (analogRead(A3)>sen) || (analogRead(A4)>sen) || (analogRead(A5)>sen)){
      key=0;
      delay(20);
      i=14;
      j=1;
      for(i;i<20;i=i+1,j=j*2){
        if(analogRead(i)>sen){key=key+j;}
      }
      b_ctrl=digitalRead(CTRL_port);
      b_shift=digitalRead(SHIFT_port);
      //while((analogRead(A0)>sen) || (analogRead(A1)>sen) || (analogRead(A2)>sen) || (analogRead(A3)>sen) || (analogRead(A4)>sen) || (analogRead(A5)>sen)){}
      last_millis=millis();
    }
  return key;
}
byte get_code(byte key, bool b_caps){
  byte code=key-1;
  if(key>36){
    code=code+72;
    if((b_ctrl==0) && (b_shift==1)){code=code+8;}
    else if((b_ctrl==1) && (b_shift==0)){code=code+16;}
    else if((b_ctrl==1) && (b_shift==1)){code=code+24;}
  }
  else{
    if(b_caps==1){
       if((b_ctrl==0) && (b_shift==0)){code=code+36;}
       else if((b_ctrl==0) && (b_shift==1)){}
       else if((b_ctrl==1) && (b_shift==0)){code=code+72;}
       else if((b_ctrl==1) && (b_shift==1)){code=code+140;}
    }
    else{
      if((b_ctrl==1) && (b_caps==1)){code=code+72;}
      else if((b_ctrl==1) && (b_shift==0)){code=code+140;}
      else if((b_ctrl==0) && (b_shift==1)){code=code+36;}
      else if((b_ctrl==1) && (b_shift==1)){code=code+72;}
    }
  }
return code;
}

void send_serial(byte send_code){
  int a=7;
  bool b_x;
  long timeout=millis();
  for(a; a>=0; a--){
    if(send_code>=(1 << a)){b_x=1;}
    else{b_x=0;}
    pinMode(p_serial_1, b_x);
    pinMode(p_serial_0, !b_x);
    digitalWrite(p_serial[b_x], 1);
    send_code=send_code-(1 << a)*b_x;
    while(digitalRead(p_serial[!b_x])==0){if(millis()-timeout>t_timeout){return;}}
    digitalWrite(p_serial[b_x], 0);
  }
}



