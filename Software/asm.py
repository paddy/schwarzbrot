#!/usr/bin/env python3

import argparse
import enum
import re
import sys

from typing import (
    Any,
    BinaryIO,
    cast,
    Dict,
    List,
    Optional,
    Type,
    Union,
)

class Register(enum.Enum):
    zero = 0
    a = 1
    b = 2
    c = 3
    d = 4
    sp = 5
    ds = 6
    io = 7

class OperandError(Exception):
    pass

class Operand:
    def __init__(self) -> None:
        pass

    @staticmethod
    def from_str(s: str) -> 'Operand':
        if s.startswith('%'):
            try:
                reg = Register[s[1:]]
                return RegisterOperand(reg)
            except KeyError:
                raise OperandError(f"Unbekanntes Register: '{s}'")
        elif re.fullmatch(r"[0-9]+|0x[0-9A-Fa-f]+", s):
            value = int(s, 0)
            return ImmediateOperand(value)
        elif re.fullmatch(r"'.'", s):
            value = ord(s[1])
            return ImmediateOperand(value)
        elif re.fullmatch(r"[a-z_:]+", s):
            return LabelOperand(s)
        else:
            raise OperandError(f"Ungültiger Operand: '{s}'")

class RegisterOperand(Operand):
    def __init__(self, reg: Register) -> None:
        self.reg = reg

class ImmediateOperand(Operand):
    def __init__(self, value: int) -> None:
        self.value = value

class LabelOperand(ImmediateOperand):
    def __init__(self, label: str) -> None:
        self.label = label

class Flags(enum.Enum):
    ZF1 = 0x01
    CF1 = 0x02
    SF1 = 0x04
    ZF0 = 0x08
    CF0 = 0x10
    SF0 = 0x20

class Opcode(int, enum.Enum):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__()
        self.mnemonic: str
        self.op_a: Union[Type[Operand], Type[None]]
        self.op_b: Union[Type[Operand], Type[None]]
        self.bits: int

    def __new__(cls, opcode: int, mnemonic: str,
                op_a: Optional[Type[Operand]],
                op_b: Optional[Type[Operand]],
                bits: int = 0) -> 'Opcode':
        value = len(cls.__members__) + 1
        obj = cast('Opcode', int.__new__(cls, value)) # type: ignore
        obj._value_ = value # type: ignore
        obj.opcode = opcode
        obj.mnemonic = mnemonic
        obj.op_a = op_a or type(None)
        obj.op_b = op_b or type(None)
        obj.bits = bits
        return obj

    @classmethod
    def find_matching(cls, mnemonic: str,
                      op_a: Optional[Operand],
                      op_b: Optional[Operand]) -> 'Opcode':
        for o in cls:
            if (o.mnemonic == mnemonic
                    and isinstance(op_a, o.op_a)
                    and isinstance(op_b, o.op_b)):
                return o

        raise Exception('%s %s, %s ist keine gültige Instruktion' %
                        (mnemonic, op_a, op_b))

    NOP    = (0x19, 'nop', None, None)
    LDi    = (0x01, 'ld',     RegisterOperand, ImmediateOperand)
    STi    = (0x02, 'st',     RegisterOperand, ImmediateOperand)
    MOV    = (0x03, 'mov',    RegisterOperand, RegisterOperand)
    JMPr   = (0x04, 'jmp',    RegisterOperand, RegisterOperand)
    JMPi   = (0x05, 'jmp',    RegisterOperand, ImmediateOperand)
    ADDr   = (0x06, 'add',    RegisterOperand, RegisterOperand)
    ADDi   = (0x07, 'add',    RegisterOperand, ImmediateOperand)
    SUBr   = (0x08, 'sub',    RegisterOperand, RegisterOperand)
    SUBi   = (0x09, 'sub',    RegisterOperand, ImmediateOperand)
    ANDr   = (0x0a, 'and',    RegisterOperand, RegisterOperand)
    ANDi   = (0x0b, 'and',    RegisterOperand, ImmediateOperand)
    NANDr  = (0x0c, 'nand',   RegisterOperand, RegisterOperand)
    NANDi  = (0x0d, 'nand',   RegisterOperand, ImmediateOperand)
    ORr    = (0x0e, 'or',     RegisterOperand, RegisterOperand)
    ORi    = (0x0f, 'or',     RegisterOperand, ImmediateOperand)
    NORr   = (0x10, 'nor',    RegisterOperand, RegisterOperand)
    NORi   = (0x11, 'nor',    RegisterOperand, ImmediateOperand)
    NOTr   = (0x12, 'not',    RegisterOperand, RegisterOperand)
    NOTi   = (0x13, 'not',    RegisterOperand, ImmediateOperand)
    LDI    = (0x14, 'ldi',    RegisterOperand, ImmediateOperand)
    LDr    = (0x15, 'ld',     RegisterOperand, RegisterOperand)
    STr    = (0x16, 'st',     RegisterOperand, RegisterOperand)

    IF_Z   = (0x17, 'if.z',   None, None, 0xff - Flags.ZF1.value)
    IF_NZ  = (0x17, 'if.nz',  None, None, 0xff - Flags.ZF0.value)
    IF_EQ  = (0x17, 'if.eq',  None, None, 0xff - Flags.ZF1.value)
    IF_NEQ = (0x17, 'if.neq', None, None, 0xff - Flags.ZF0.value)
    IF_GT  = (0x17, 'if.gt',  None, None, 0xff - (Flags.CF0.value or Flags.ZF0.value))
    IF_GE  = (0x17, 'if.ge',  None, None, 0xff - Flags.CF0.value)
    IF_LT  = (0x17, 'if.lt',  None, None, 0xff - Flags.CF1.value)
    IF_C   = (0x17, 'if.c',   None, None, 0xff - Flags.CF1.value)
    IF_NC  = (0x17, 'if.nc',  None, None, 0xff - Flags.CF0.value)

class Instruction:
    def __init__(self, source_file: str, line: int, opcode: Opcode,
                 op_a: Optional[Operand], op_b: Optional[Operand]) -> None:
        self.source_file = source_file
        self.line = line
        self.opcode = opcode
        self.op_a = op_a
        self.op_b = op_b

    def size(self) -> int:
        return 2

    def get_bytes(self, labels: Dict[str, int]) -> bytes:
        opcode = self.opcode.opcode
        assert opcode < 32

        if self.op_a:
            assert isinstance(self.op_a, RegisterOperand)
            reg1 = self.op_a.reg.value
            assert reg1 < 8
        else:
            reg1 = 0

        byte1 = opcode | (reg1 << 5)

        if isinstance(self.op_b, RegisterOperand):
            byte2 = self.op_b.reg.value
            assert byte2 < 8
            assert self.opcode.bits == 0
        elif isinstance(self.op_b, ImmediateOperand):
            if isinstance(self.op_b, LabelOperand):
                label = self.op_b.label
                if label not in labels:
                    if label + ":ofs" in labels:
                        raise Exception(f"Label {label} benötigt :seg oder :ofs")
                    else:
                        raise Exception(f"Unbekanntes Label {label}")
                value = labels[label]
            else:
                value = self.op_b.value
            byte2 = value
            assert self.opcode.bits == 0
        else:
            byte2 = self.opcode.bits

        return bytes((byte1, byte2))

    def write(self, f: BinaryIO, labels: Dict[str, int]) -> None:
        f.write(self.get_bytes(labels))

def strip_comments(line: str) -> str:
    comment = line.find(';')
    if comment >= 0:
        line = line[0:comment]
    return line.strip()

argp = argparse.ArgumentParser(description='Schwarzbrot-Assembler')
argp.add_argument('input', metavar='INPUT',
                  help='Dateiname der Eingabedatei (*.asm)')
argp.add_argument('-c', '--c-output', action='store_true',
                  help='C-Bytearray statt Binärdatei erzeugen')

args = argp.parse_args()

in_filename = args.input

out_ext = ".c" if args.c_output else ".bin"
if in_filename.endswith(".asm"):
    out_filename = in_filename[0:-4] + out_ext
else:
    out_filename = in_filename + out_ext

src = [strip_comments(x) for x in open(in_filename, 'r')]
instructions: List[Instruction] = []
labels: Dict[str, int] = {}
pos = 0

for i, line in enumerate(src, start=1):
    if not line:
        continue

    try:
        match = re.fullmatch(r"([a-z_]+):", line)
        if match:
            label_name = match.group(1)
            if label_name + ":ofs" in labels:
                raise Exception(f"Label '{label_name}' existiert schon")
            labels[label_name + ":ofs"] = pos & 0xff
            labels[label_name + ":seg"] = pos >> 8
            continue

        match = re.fullmatch(r"([a-z.]+)(?: ([%a-z]+)(?:, ([%' A-Za-z_:0-9]+))?)?", line)
        if not match:
            raise Exception(f"Falsches Befehlsformat: '{line}'")

        mnemonic, op_a_str, op_b_str = match.groups()
        op_a = Operand.from_str(op_a_str) if op_a_str else None
        op_b = Operand.from_str(op_b_str) if op_b_str else None

        opcode = Opcode.find_matching(mnemonic, op_a, op_b)
        instr = Instruction(in_filename, i, opcode, op_a, op_b)
        instructions.append(instr)
        pos += instr.size()

    except Exception as e:
        print(f"{in_filename}:{i}: {e}")
        sys.exit(1)

if args.c_output:
    with open(out_filename, 'wt') as f:
        byte_array = bytes()
        for instr in instructions:
            try:
                byte_array += instr.get_bytes(labels)
            except Exception as e:
                print(f"{instr.source_file}:{instr.line}: {e}")
                sys.exit(1)
        f.write("const unsigned char schwarzbrot_bin[] = {")
        f.write(','.join(("\n    " if i % 8 == 0 else " ") + f'0x{b:02x}'
                          for i, b in enumerate(byte_array)))
        f.write("\n};\n")
else:
    with open(out_filename, 'wb') as bf:
        for instr in instructions:
            try:
                instr.write(bf, labels)
            except Exception as e:
                print(f"{instr.source_file}:{instr.line}: {e}")
                sys.exit(1)
