#define p_pulse_out 2
#define p_speed A5
#define p_single_pulse A1
#define p_mode A0
#define p_reset_in A2
#define p_reset_out 3
#define res_delay 100
#define delay_single_pulse 200
long freq;
long pulse_delay;
long ts;
long multiplier = 1000000;
long max_freq = 1000;
bool edge = 0;
void setup() {
  pinMode(p_pulse_out,1);
  pinMode(p_reset_out,1);
  digitalWrite(p_reset_out, 1);
 // Serial.begin(9600);
  ts = micros();
}

void loop() {
  freq = map(analogRead(p_speed), 0, 1010, 1, max_freq);
  pulse_delay = multiplier / freq;
  //pulse_delay = 1000;
  if (pulse_delay == 0){
    pulse_delay = multiplier/max_freq;
    if(pulse_delay == 0){
      pulse_delay = 1;
    }
  }
  if(analogRead(p_mode) >500){
    if(micros() - ts >= pulse_delay){
      digitalWrite(p_pulse_out, !edge);
      edge = !edge;
      ts = micros();
    }
  }
  else if(analogRead(p_single_pulse) >500){
    digitalWrite(p_pulse_out, 1);
    delay(delay_single_pulse);
    digitalWrite(p_pulse_out, 0);
    while( analogRead(p_single_pulse) >500 ){}
    delay(delay_single_pulse);
  }
  if(analogRead(p_reset_in) >500){
    reset();
  }
  
}

void reset(){
  if(analogRead(p_reset_in) > 500){  
    digitalWrite(p_reset_out, 0);
    digitalWrite(p_pulse_out, 1);
    delay(res_delay);
    digitalWrite(p_pulse_out, 0);
    delay(res_delay);
    digitalWrite(p_pulse_out, 1);
    delay(res_delay);
    digitalWrite(p_pulse_out, 0);
    delay(res_delay);
    digitalWrite(p_reset_out, 1);
  }
}
