#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define sen 500
#define t_safe 50
#define s_space 44
#define space 109
#define CTRL_port 7
#define SHIFT_port 6
#define p_serial_0 9
#define p_serial_1 10
#define t_timeout 100

const byte p_serial[2] = { p_serial_0, p_serial_1 };

const char kc_to_ascii[] =
    /*   0 */ "1234567890"
    /*  10 */ "qwertzuiop"
    /*  20 */ "asdfghjkly"
    /*  30 */ "xcvbnm!\"'4"
    /*  40 */ "%&/()=QWER"
    /*  50 */ "TZUIOPASDF"
    /*  60 */ "GHJKLYXCVB"
    /*  70 */ "NM1^34567?"
    /*  80 */ "9}@we,;.:*"
    /*  90 */ "_-+~#|ghjk"
    /* 100 */ "lyxcvbnm\b\n"
    /* 110 */ "           ";
       
void setup(void)
{
    lcd.begin(16,2);
    Serial.begin(9600);
}

void loop(void)
{
    int code;
    int t = millis();

    code = get_code();
    if (code >= 0) {
        Serial.print("Empfangen: ");
        Serial.println(code);
        if (code < sizeof(kc_to_ascii)) {
            Serial.println(kc_to_ascii[code]);
        }
        Serial.print(millis()-t);
        Serial.println(" ms");
    } else {
        Serial.print("Timeout ");
        Serial.println(code);
    }
}

static int get_bit(void)
{
    if (digitalRead(p_serial[0])) {
        return 0;
    }
    if (digitalRead(p_serial[1])) {
        return 1;
    }
    return -1;
}

int get_code(void)
{
    long t_start = millis();
    int i;
    int bit;
    int result = 0;

    pinMode(p_serial_0, 0);
    pinMode(p_serial_1, 0);

    for (i = 7; i >= 0; i--) {
        while ((bit = get_bit()) == -1) {
            if (millis() - t_start > t_timeout) {
                return -1;
            }
        }
        result |= (bit << i);

        pinMode(p_serial[!bit], 1);
        digitalWrite(p_serial[!bit], 1);

        while (digitalRead(p_serial[bit])) {
            if (millis() - t_start > t_timeout) {
                return -2;
            }
        }

        digitalWrite(p_serial[!bit], 0);
        pinMode(p_serial[!bit], 0);
    }

    return result;
}
