    mov %a, %zero
    mov %b, %zero
add:
    add %a, %b
    if.nz
    jmp %zero, err:ofs

    add %a, 1
    if.z
    jmp %zero, err:ofs

    sub %b, %a
    if.ge
    jmp %zero, err:ofs

    mov %b, %zero
    sub %a, %b
    if.lt
    jmp %zero, err:ofs

    mov %b, %a
    if.gt
    jmp %zero, err:ofs

err:
    ldi %a, 255
    jmp %zero, err:ofs
