    mov %b, %zero
    mov %ds, %zero

loop:
    ld %a, %b
    add %b, 1
    jmp %zero, loop:ofs
