loop:
    ldi %a, 5
    mov %ds, %zero
    st %a, %zero

    ldi %a, 42
    ldi %b, 42
    st %a, %b

    mov %a, %zero

    ld %a, %zero
    ld %a, %b

    jmp %zero, loop:ofs
