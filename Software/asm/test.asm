    nop
    jmp %zero, start:ofs
 
; I/O
    mov %a, %io
    jmp %zero, start:ofs

; Alle Register schreiben
    mov %b, %a
    mov %c, %a

    mov %ds, %a
    mov %c, %a

    mov %sp, %a
    mov %c, %a

    mov %c, %a
    mov %c, %c

    mov %d, %a
    mov %c, %a

    mov %io, %a
    mov %c, %a

    jmp %zero, start:ofs

; Alle Register lesen
    mov %a, %b
    mov %a, %ds
    mov %a, %sp
    mov %a, %c
    mov %a, %d
    mov %a, %io
    jmp %zero, start:ofs

; Bitshift
start:
    ldi %a, 1
    or %zero, 0
loop:
    mov %b, %a
    mov %ds, %a
    mov %sp, %a
    mov %c, %a
    mov %d, %a
    mov %io, %a
    if.c
    jmp %zero, start:ofs
    add %a, %a
    jmp %zero, loop:ofs
