    ldi %a, 0xff
    ldi %b, 255 ; Eigentlich unnötig
    nand %a, 255

    not %a, 42
    not %b, %a
    and %a, 63
    nor %a, %b
    or %b, %a
    sub %b, 42
    nand %a, %b
