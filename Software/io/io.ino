#define p_write 10
#define p_read 11
#define p_clk 12
#define p_data 2

byte curr_data = 0;
byte out_data = 0;
bool clk;
bool old_read;
int read_state = 0;

void setup(void)
{
    pinMode(p_write, 0);
    pinMode(p_read, 0);
    pinMode(p_clk, 0);
    clk = digitalRead(p_clk);
    Serial.begin(9600);
    Serial.println("Serielle Schnittstelle bereit");
}

void loop(void)
{
    bool is_write = digitalRead(p_write);
    bool is_read = digitalRead(p_read);
    bool curr_clk = digitalRead(p_clk);

#if 0
    if (is_read && !old_read) {
        Serial.println("read -> 1");
        Serial.println(read_state);
    } else if (!is_read && old_read) {
        Serial.println("read -> 0");
        Serial.println(read_state);
    }
#endif

    if (is_write) {
        set_pins(0);
        curr_data = read_data();
        if (!clk && curr_clk) {
            Serial.print("Wert: ");
            Serial.println(curr_data);
        }
    }

    switch (read_state) {
        case 0:
            if (is_read) {
                set_pins(1);
                write_data(out_data);         
            }
            if (is_read && !clk && curr_clk) {
                Serial.println("read && Steigende Flanke");
                read_state = 1;
                old_read = is_read;

            }
            break;
        case 1:
            if (old_read && clk && !curr_clk) {
                read_state = 0;
                old_read = is_read;
                out_data++;
                Serial.print("Naechstes Read: ");
                Serial.println(out_data);
            }
            break;
    }

#if 0
    if (clk != curr_clk) {
        Serial.print("Takt: ");
        Serial.println(curr_clk);
    }
#endif

    clk = curr_clk;
}

void set_pins(bool mode)
{
    for (int i = p_data; i < p_data+8; i++) {
        pinMode(i, mode);  
    } 
}

byte read_data()
{
    byte data = 0;
    for (int i = 0; i < 8; i++) {
        data |= digitalRead(i+p_data) << i;
    }
    return data;
}


void write_data(byte data)
{
    for (int i = 0; i < 8; i++) {
        digitalWrite(i+p_data, data & (1 << i));
    }
}
